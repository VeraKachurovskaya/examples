import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguagesService } from '../../services/languages.service';
import { FormatUrlService } from '../../services/format-url.service';
import { PurchasesService } from '../../services/purchases.service';
import { StorageService } from '../../services/storage.service';
import { DataService } from '../../services/data.service';

declare var window: any;
declare var SpinnerDialog: any;
declare var device: any;

@Component({
  selector: 'app-premium-access',
  templateUrl: './premium-access.component.html',
  styleUrls: ['./premium-access.component.scss']
})
export class PremiumAccessComponent implements OnInit {
  public title: String = 'Premium Access';
  public lang = {
    title: '',
    description: '',
    annual_button: '',
    monthly_button: '',
    annual: '',
    ann_billing: '',
    recurring: '',
    cancel: '',
    month_billing: '',
    purchase: '',
    sub_description: '',
    bottom_description: '',
    currency_month: '',
    currency_year: ''
  };

  public toggleBilling = false;
  public prevPage: any;
  public sub: any;
  public currentPage: any;
  // public subscribe_info: any;
  public subscribe_info = this.dataService.subscribe_info
    ? JSON.parse(JSON.stringify(this.dataService.subscribe_info))
    : [];
  public idSubscribe: String = this.subscribe_info.annual_id; // default value 'strongmom.com.annual'
  public isEnLang: Boolean = this.dataService.is_en_language ? true : false;
  constructor(
    public route: ActivatedRoute,
    public languagesService: LanguagesService,
    private formatUrlService: FormatUrlService,
    public router: Router,
    public purchases: PurchasesService,
    public storageService: StorageService,
    public dataService: DataService
  ) {}

  ngOnInit() {
    this.lang = this.languagesService.getLanguages(false, 'PremiumAccess');
    this.sub = this.route.params.subscribe(params => {
      this.prevPage = this.formatUrlService.formatUrl(params.prev_page);
    });

    // this.storageService.getItem('subscribe_info').then((result: any) => {
    //   this.subscribe_info = JSON.parse(result);
    // });
  }

  back() {
    const link = this.prevPage || '/program-exercises';
    this.router.navigateByUrl(link);
  }

  changePeriod(event: any) {
    if (document.getElementsByClassName('active')[0]) {
      document.getElementsByClassName('active')[0].classList.remove('active');
    }
    if (event.target.className === 'monthly-button') {
      this.toggleBilling = true;
      this.idSubscribe = this.subscribe_info.mounth_id; // monthly subscribe
    } else {
      this.toggleBilling = false;
      this.idSubscribe = this.subscribe_info.annual_id; // annualy subscribe
    }
    event.target.classList.add('active');
  }

  subscribePrograms() {
    if (window.cordova) {
      SpinnerDialog.show();
      let productId = this.idSubscribe;
      if (device.platform === 'iOS') {
        productId = 'ios.' + this.idSubscribe;
      }
      this.purchases
        .loadProducts([productId])
        .then(response => {
          this.purchases
            .subscribe(response[0].productId)
            .then(subscribeResponse => {
              SpinnerDialog.hide();
            })
            .catch(subscribeErr => {
              SpinnerDialog.hide();
            });
          SpinnerDialog.hide();
        })
        .catch(subscribeErr => {
          SpinnerDialog.hide();
        });
    }
  }
}
