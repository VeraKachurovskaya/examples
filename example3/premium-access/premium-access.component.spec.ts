import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumAccessComponent } from './premium-access.component';

describe('PremiumAccessComponent', () => {
  let component: PremiumAccessComponent;
  let fixture: ComponentFixture<PremiumAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
