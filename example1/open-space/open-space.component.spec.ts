import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenSpaceComponent } from './open-space.component';

describe('OpenSpaceComponent', () => {
  let component: OpenSpaceComponent;
  let fixture: ComponentFixture<OpenSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenSpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
