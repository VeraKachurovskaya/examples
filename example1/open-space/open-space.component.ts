import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICard } from '../../models/card';
import { HttpService } from '../../services/http.service';
import { SomeError } from '../../models/error';
import { SetError } from '../../store/reducers/error-action';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { MR } from '../../models/meeting-room';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-open-space',
  templateUrl: './open-space.component.html',
  styleUrls: ['./open-space.component.scss'],
})
export class OpenSpaceComponent implements OnInit, OnDestroy {
  public uhoo_analytics: ICard[] = [
    // {
    //   title: 'Humidity',
    //   key: 'humidity',
    //   unit_of_measure: '%',
    //   limits_exceeded: 12,
    //   start_scale: 0,
    //   end_scale: 100,
    //   max_normal: 50,
    //   min_normal: 30,
    //   lower_norm_offset: 30,
    //   higher_norm_offset: 50,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
    // {
    //   title: 'Temperature',
    //   key: 'temperature',
    //   unit_of_measure: 'C',
    //   limits_exceeded: 12,
    //   start_scale: 10,
    //   end_scale: 40,
    //   max_normal: 26,
    //   min_normal: 21,
    //   lower_norm_offset: 10,
    //   higher_norm_offset: 14,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
    // {
    //   title: 'CO2',
    //   key: 'co2',
    //   unit_of_measure: 'ppm',
    //   limits_exceeded: 12,
    //   start_scale: 400,
    //   end_scale: 2500,
    //   min_normal: 400,
    //   max_normal: 800,
    //   lower_norm_offset: 0,
    //   higher_norm_offset: 700,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
    // {
    //   title: 'TVOC',
    //   key: 'tvoc',
    //   unit_of_measure: 'ppb',
    //   limits_exceeded: 12,
    //   start_scale: 0,
    //   end_scale: 1100,
    //   min_normal: 0,
    //   max_normal: 400,
    //   lower_norm_offset: 0,
    //   higher_norm_offset: 400,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
    // {
    //   title: 'PM2.5',
    //   key: 'pm_2_5',
    //   unit_of_measure: 'µg/m³',
    //   limits_exceeded: 12,
    //   start_scale: 0,
    //   end_scale: 200,
    //   min_normal: 50,
    //   max_normal: 100,
    //   lower_norm_offset: 0,
    //   higher_norm_offset: 100,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
    // {
    //   title: 'Ozone',
    //   key: 'ozone',
    //   unit_of_measure: 'ppb',
    //   limits_exceeded: 12,
    //   start_scale: 10,
    //   end_scale: 100,
    //   min_normal: 10,
    //   max_normal: 30,
    //   lower_norm_offset: 0,
    //   higher_norm_offset: 40,
    //   lowest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    //   highest: {
    //     location: 'GF',
    //     date: ' 01/03/19',
    //     value: 90,
    //   },
    // },
  ];

  public loader = false;
  public last_date = '';
  public unsubscribeOnDestroy: Subject<any> = new Subject();
  constructor(private http: HttpService, public store: Store<MR | string>) {}

  ngOnInit() {
    this.loader = true;
    this.store
      .select('date')
      .pipe(takeUntil(this.unsubscribeOnDestroy))
      .subscribe((lastDate: string) => {
        this.last_date = lastDate;
        this.http.getUhooSensorData(lastDate).subscribe(
          (successResponce: any) => {
            if (successResponce.data) {
              this.uhoo_analytics = successResponce.data;
            } else {
              this.uhoo_analytics = [];
            }
            if (successResponce.status === -1) {
              const requestError = new SomeError(successResponce.msg, true);
              this.store.dispatch(new SetError(requestError));
            }
            this.loader = false;
          },
          errorResponce => {
            console.log('errorResponce', errorResponce);
            this.loader = false;
            const requestError = new SomeError('Request error. Contact to your support', true);
            this.store.dispatch(new SetError(requestError));
          },
        );
      });
  }
  ngOnDestroy(): void {
    this.unsubscribeOnDestroy.next();
    this.unsubscribeOnDestroy.complete();
  }
}
