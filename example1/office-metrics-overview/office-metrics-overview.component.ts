import { Component, OnInit} from '@angular/core';
import { HttpService } from '../../services/http.service';
import * as moment from 'moment';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-office-metrics-overview',
  templateUrl: './office-metrics-overview.component.html',
  styleUrls: ['./office-metrics-overview.component.scss']
})

export class OfficeMetricsOverviewComponent implements OnInit {
  moment = moment;

  value = 0;
  period;
  parametrsForInterval = { // in the minutes
    'Yesterday': {
      'min': 60, // 1 hours
      'max' : 360, // 6 hours
      'step' : 60,
    },
    'Last 24 hours': {
      'min': 60,
      'max' : 360,
      'step' : 60,
    },
    'Last 12 hours': {
      'min': 15,
      'max' : 180, // 3 hours
      'step' : 15,
    },
    'Last 6 hours': {
      'min': 10,
      'max' : 90,
      'step' : 10,
    },
    'Last 3 hours': {
      'min': 5,
      'max' : 45,
      'step' : 5,
    },
    'Last hour': {
      'min': 1,
      'max' : 15,
      'step' : 1,
    },
    'Custom': {
      'min': 60,
      'max' : 360,
      'step' : 60,
    },
  };
  currentState = {
    'min': 60, // 1 hours
    'max' : 360, // 6 hours
    'step' : 60,
  };
  currentPeriod;
  rangeDates: Date[];
  // rangeDates: string;
  currentDay: Date;

  // for request
  start;
  end;
  breakdownInterval;
  type = [
    'net_occupancy',
    'footfall'
  ];
  currentType = this.type[0];

  responce = { intervals: [], stats: {}};

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    legend: { position: 'bottom'}
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [], label: '', backgroundColor: '' },
  ];

  public isFootfall = false;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.currentDay = new Date();
    this.value = this.currentState.min;

    const dayBeforeYesterday = moment().utc().subtract(2, 'day').endOf('day');
    const yesterday = moment().utc().startOf('day');
    this.start = dayBeforeYesterday.toISOString();
    this.end = yesterday.toISOString();
    this.getData();
  }

  updateSetting(event) {
    this.value = event.value;

    this.getData();
  }

  selectOption(id: string) {
    this.currentState = this.parametrsForInterval[id];
    this.value = this.parametrsForInterval[id].min;
    this.currentPeriod = id;

    switch (id) {
      case 'Yesterday': {
        const dayBeforeYesterday = moment().utc().subtract(2, 'day').endOf('day');
        const yesterday = moment().utc().startOf('day');
        this.start = dayBeforeYesterday.toISOString();
        this.end = yesterday.toISOString();
        break;
      }
      case 'Last 24 hours':  {
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        this.start = yesterday.toISOString();
        this.end = new Date().toISOString();
        break;
      }
      case 'Last 12 hours':  {
        this.start = moment().utc().subtract(12, 'hour').toISOString();
        this.end = moment().utc().toISOString();
        break;
      }
      case 'Last 6 hours':  {
        this.start = moment().utc().subtract(6, 'hour').toISOString();
        this.end = moment().utc().toISOString();
        break;
      }
      case 'Last 3 hours':  {
        this.start = moment().utc().subtract(3, 'hour').toISOString();
        this.end = moment().utc().toISOString();
        break;
      }
      case 'Last hour':  {
        this.start = moment().utc().subtract(1, 'hour').toISOString();
        this.end = moment().utc().toISOString();
        break;
      }
      default: {
      }
    }
    this.getData();
  }

  selectDate() {
    const day_start = this.rangeDates[0].getDate();
    const month_start = this.rangeDates[0].getMonth();
    const year_start = this.rangeDates[0].getFullYear();

    const day_end = this.rangeDates[1].getDate();
    const month_end = this.rangeDates[1].getMonth();
    const year_end = this.rangeDates[1].getFullYear();

    this.start = new Date(Date.UTC(year_start, month_start, day_start)).toISOString();
    this.end = new Date(Date.UTC(year_end, month_end, day_end)).toISOString();

    this.getData();
  }

  isEmpty(obj) {
    return !obj.stats || Object.keys(obj.stats).length === 0;
  }

  getData() {
    this.responce = { intervals: [], stats: [{}] };
    let options = {};
    if (this.currentType === 'net_occupancy') {
      options = {
        start: this.start,
        end: this.end,
        interval: this.value,
        request_type: 'net-occupancy'
      };
    } else {
      options = {
        start: this.start,
        end: this.end,
        interval: this.value
      };
    }
    this.http.getUbiqi(options).subscribe(
      (data) => {
        this.responce = data;
        this.makeChartData(this.responce.intervals);
    });
  }

  makeChartData(data: any) {
    if (this.currentType === 'net_occupancy') {
      this.barChartData = [
        { data: [], label: '', backgroundColor: '' },
        { data: [], label: '', backgroundColor: '' },
      ];
      this.barChartLabels = [];
      this.barChartData[0].label = 'Entered';
      for (let i = 0; i < data.length; i++) {
        this.barChartLabels.push(moment(data[i].start).utc().format('HH:mm') + '-' + moment(data[i].end).utc().format('HH:mm'));
        this.barChartData[0].data.push(Math.abs(data[i].maxOccupancy));
        this.barChartData[0].backgroundColor = '#3997D3';
        this.barChartData[1].backgroundColor = '#fff';
        // this.chartColors[0].backgroundColor[i] = 'rgba(63, 191, 127,  0.4)';
      }
    } else {
      this.barChartData = [
        { data: [], label: '', backgroundColor: '' },
        { data: [], label: '', backgroundColor: '' },
      ];
      this.barChartLabels = [];
      this.barChartData[0].label = 'Entered';
      this.barChartData[1].label = 'Exited';
      for (let i = 0; i < data.length; i++) {
        this.barChartLabels.push(moment(data[i].start).utc().format('HH:mm') + '-' + moment(data[i].end).utc().format('HH:mm'));
        if (data[i].lines.length) {
          this.barChartData[0].data.push(Math.abs(data[i].lines[0].entered));
          this.barChartData[1].data.push(Math.abs(data[i].lines[0].exited));
          this.barChartData[0].backgroundColor = '#3997D3';
          this.barChartData[1].backgroundColor = '#1B5479';
        } else {
          this.barChartData[0].data.push(0);
          this.barChartData[1].data.push(0);
          this.barChartData[0].backgroundColor = '#3997D3';
          this.barChartData[1].backgroundColor = '#1B5479';
        }
      }
    }
  }

  showFootfall(show: boolean): void {
    this.isFootfall = show;
    if (this.isFootfall === true) {
      this.currentType = this.type[1];
    } else {
      this.currentType = this.type[0];
    }
    this.getData();
  }

}

