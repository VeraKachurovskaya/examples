import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeMetricsOverviewComponent } from './office-metrics-overview.component';

describe('OfficeMetricsOverviewComponent', () => {
  let component: OfficeMetricsOverviewComponent;
  let fixture: ComponentFixture<OfficeMetricsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeMetricsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeMetricsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
