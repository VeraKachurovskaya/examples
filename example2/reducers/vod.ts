// tslint:disable: max-classes-per-file
import { Action, createFeatureSelector } from '@ngrx/store';

export const initialState = {
  movie_id: null,
  title: '',
  description: '',
  cover_url: '',
  trailer_url: '',
  is_hd: null,
  catalog_date_start: '',
  production_year: null,
  director_name: '',
  languages: [],
  subtitles: [],
  categories: [],
  actors: [],
  price: null
};

export enum ActionTypes {
  result = '[vod] result',
  error = '[vod] error'
}

export class GetVODResult implements Action {
  readonly type = ActionTypes.result;
  constructor(public payload: typeof initialState) {}
}

export class GetVODError implements Action {
  readonly type = ActionTypes.error;
  constructor(public payload: typeof initialState) {}
}

export const getProductsState = createFeatureSelector<ActionTypes>('vod');

export function getVOD(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.result:
      return action.payload;
    case ActionTypes.error:
      return { ...initialState };
    default:
      return state;
  }
}
