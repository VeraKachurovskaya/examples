import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment-timezone';
import { URLS } from '../../models/urls';
import { HttpService } from '../../services/http.service';
import { Store } from '@ngrx/store';
import { MetaService } from '@ngx-meta/core';
import { GetChannelError, GetChannelResult } from '@reducers/channel';
import { ActivatedRoute, Router } from '@angular/router';

const moment = extendMoment(Moment);
let positionRunner = 0;

const watchRunner = setInterval(() => {
  if (document.querySelector('.program-block')) {
    const padding = 15; // padding of .layout.ng-star-inserted element
    const widthUnusedHour = 300;
    const elProgram = document.querySelector('.program-block');
    const elRunner = document.querySelector('.line-runner');

    const width = elProgram.scrollWidth - widthUnusedHour + padding;
    const hours =
      +moment
        .utc()
        .add(1, 'hour')
        .format('HH') || 0;
    const minutes = +moment.utc().format('mm') / 60 || 0;
    const position = ((hours + minutes) / 23.88) * width;
    positionRunner = Math.round(position);

    const scrollPosition = positionRunner - elProgram.clientWidth / 2 || 0;
    const currentTime = moment
      .utc()
      .tz('Europe/Berlin')
      .format('HH:mm');
    if (elRunner.querySelector('.current-time').innerHTML !== currentTime) {
      elRunner.querySelector('.current-time').innerHTML = currentTime;
    }
    elRunner.setAttribute('style', `left: ${positionRunner}px`);
    if (!elProgram.getAttribute('data-scroll-done')) {
      // check scrollTo for IE
      if (elProgram.scrollTo) {
        elProgram.scrollTo(scrollPosition, 0);
      } else {
        elProgram.scrollLeft = scrollPosition;
      }
      elProgram.setAttribute('data-scroll-done', 'true');
    }
  }
}, 2000);

@Component({
  selector: 'app-epg-block',
  templateUrl: './epg-block.component.html',
  styleUrls: ['./epg-block.component.scss']
})
export class EpgBlockComponent implements OnInit, OnChanges {
  public id;
  channelsName;
  channels = [];
  arr = [];
  currDay = {
    date: moment.utc(),
    name: moment().format('ddd D, MMMM')
  };
  public filters = [];
  public filter;
  public epg;
  public programs;
  public hours = [];
  public extended = true;
  public element;
  public currentChannel;
  public currentChannelIndex;

  @Input() selectedChannels: Array<{ display: string; value: string }> = [];

  constructor(
    public http: HttpService,
    private urls: URLS,
    private deviceService: DeviceDetectorService,
    private meta: MetaService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public store: Store<any>
  ) { }

  @ViewChild('timeline', { read: ElementRef, static: false }) public timeline: ElementRef<any>;

  ngOnInit() {
    for (let i = 0; i <= 24; i++) {
      this.hours.push(i);
    }
    this.filters = this.getFilter(2, 5);
    this.sortByDayTime(this.currDay.date);
  }

  public currentProgram(item) {
    const start = moment.utc(item.start);
    const stop = moment.utc(item.stop);
    const now = moment.utc().add(1, 'hour');
    return now.isBetween(start, stop);
  }

  public timeLeft(item) {
    const stop = moment.utc(item.stop);
    const now = moment.utc().add(1, 'hour');
    return Math.round(moment.duration(stop.diff(now)).asMilliseconds() / 60000);
  }

  public UTCTime(item) {
    return moment.utc(item).format('HH:mm');
  }

  public currentTime() {
    return moment
      .utc()
      .tz('Europe/Berlin')
      .format('HH:mm');
  }

  calculateWidth(item) {
    return (item * 100) / 1500;
  }

  checkMobile() {
    return this.deviceService.isMobile();
  }

  storeData(id) {
    this.http.getData(this.urls.channel + id).subscribe(
      result => {
        this.id = result.id;
        this.meta.setTitle(`PostTV -  ${result.name}`);
        this.meta.setTag('og:url', window.location.href);
        this.meta.setTag('og:description', result.description);
        this.meta.setTag('og:image', result.logo[0].url);
        this.programs = result.epg.programme;
        this.store.dispatch(new GetChannelResult(result));
      },
      error => {
        this.store.dispatch(new GetChannelError(error));
      }
    );
  }

  sortByDayTime(selectedDay) {
    this.channels = [];
    let res = [];
    this.arr = [];
    if (this.selectedChannels) {
      res = this.selectedChannels.map(channel => {
        return this.http.getRequestChannel(
          this.urls.channel,
          channel.display,
          this.extended,
          moment(selectedDay).format('YYYY-MM-DD')
        );
      });
      Promise.all(res).then(channel => {
        channel.map(program => {
          this.channels.push(program);
          this.arr.push(Object.keys(program['epg'].programme).map(el => program['epg'].programme[el]));
        });
        if (this.checkMobile()) {
          this.currentChannel = this.channels[0];
          this.currentChannelIndex = 0;
          this.storeData(this.currentChannel.id);
        }
        this.arr.map(col => {
          for (let i = 0; i < col.length; i++) {
            if (i === 0 && col[i].start && moment.utc(col[i].start) > moment.utc(col[i].start).startOf('day')) {
              const ch = moment
                .duration(moment.utc(col[0].start).diff(moment.utc(col[0].start).startOf('day')))
                .asMilliseconds();
              col.unshift({ duration: ch / 60000 });
            } else if (i === col.length - 1) {
              if (col[i] === null && moment.utc(col[i - 1].stop).endOf('day') !== moment.utc(col[i - 1].stop)) {
                if (moment.utc(col[i - 1].stop).endOf('day').isSame(moment.utc(col[i - 1].start))) {
                  const ch = moment
                    .duration(
                      moment
                        .utc(col[i - 1].stop)
                        .endOf('day')
                        .diff(moment.utc(col[i - 1].stop))
                    )
                    .asMilliseconds();
                  col[i] = { duration: ch / 60000 };
                }
                if (!moment.utc(col[i - 1].stop).endOf('day').isSame(moment.utc(col[i - 1].start))) {
                  col[i] = { duration: 0 };
                }
              }

            }
            if (col[i] === null) {
              const ch = moment
                .duration(moment.utc(col[i + 1].start).diff(moment.utc(col[i - 1].stop)))
                .asMilliseconds();
              col[i] = { duration: ch / 60000 };
            }
          }
        });
      });
    }
  }

  public goToChannel(id_channel: string) {
    this.router.navigate(['/channel/' + id_channel], { relativeTo: this.activatedRoute });
  }

  public prevChannel() {
    this.currentChannelIndex -= 1;
    this.currentChannel = this.channels[this.currentChannelIndex];
    this.storeData(this.currentChannel.id);
  }

  public nextChannel() {
    this.currentChannelIndex += 1;
    this.currentChannel = this.channels[this.currentChannelIndex];
    this.storeData(this.currentChannel.id);
  }

  public scrollRight(): void {
    this.timeline.nativeElement.scrollTo({
      left: this.timeline.nativeElement.scrollLeft + 80,
      behavior: 'smooth'
    });
  }

  public isPrev(show) {
    const start = moment.utc(show.start);
    const now = moment.utc().add(1, 'hour');
    return now.isAfter(start);
  }

  public scrollLeft(): void {
    this.timeline.nativeElement.scrollTo({
      left: this.timeline.nativeElement.scrollLeft - 80,
      behavior: 'smooth'
    });
  }

  getFilter(start: number, end: number) {
    const dates = [];
    const startDate = moment
      .utc()
      .subtract(start, 'days')
      .startOf('day');
    const endDate = moment
      .utc()
      .add(end, 'days')
      .endOf('day');

    do {
      dates.push({
        date: moment.utc(startDate).toDate(),
        name: moment(startDate).format('ddd D, MMMM')
      });
      startDate.add(1, 'days');
    } while (startDate.diff(endDate) < 0);

    return dates;
  }

  async setTime(a) {
    this.currDay = a;
    this.programs = [];
    this.programs = this.sortByDayTime(a.date);
  }

  ngOnChanges() {
    this.setTime(this.currDay);
  }
}
