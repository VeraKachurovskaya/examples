import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TagInputModule } from 'ngx-chips';
import { URLS } from '../../models/urls';
import { HttpService } from '../../services/http.service';

TagInputModule.withDefaults({
  tagInput: {
    placeholder: '+  Add Channel'
    // add here other default values for tag-input
  }
});

@Component({
  selector: 'app-epg-filter',
  templateUrl: './epg-filter.component.html',
  styleUrls: ['./epg-filter.component.scss', './epg-tag.scss']
})
export class EpgFilterComponent implements OnInit {
  channelsName;
  searchString;
  selectedChannels: Array<{ display: string; value: string }> = [];
  channelsList: Array<{ display: string; value: string }> = [];

  @Output() channels: EventEmitter<any> = new EventEmitter();

  constructor(public http: HttpService, private urls: URLS) {}

  async ngOnInit() {
    this.channelsName = await this.http.getRequestChannelsName(this.urls.name_channel);
    if (this.channelsName) {
      for (let i = 0; i < 5; i++) {
        this.selectedChannels.push({ display: this.channelsName[i].id, value: this.channelsName[i].name });
        this.channelsList.push({ display: this.channelsName[i].id, value: this.channelsName[i].name });
      }
      for (let i = 5; i < this.channelsName.length; i++) {
        this.channelsList.push({ display: this.channelsName[i].id, value: this.channelsName[i].name });
      }
    }
    this.channels.emit(this.selectedChannels);
  }

  async filter() {}

  emitChannels() {
    this.channels.emit(this.selectedChannels);
  }
}
