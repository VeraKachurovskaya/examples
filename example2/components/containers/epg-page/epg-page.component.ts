import { Component, OnInit } from '@angular/core';
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'app-epg-page',
  templateUrl: './epg-page.component.html',
  styleUrls: ['./epg-page.component.scss']
})
export class EpgPageComponent implements OnInit {
  public selectedChannels;

  constructor(private meta: MetaService) {}

  ngOnInit() {
    this.meta.setTitle(`PostTV - EPG Guide`);
    this.meta.setTag('og:url', window.location.href);
    this.meta.setTag('og:description', '');
    this.meta.setTag('og:image', '');
  }

  handleChannels(event) {
    this.selectedChannels = event;
  }
}
